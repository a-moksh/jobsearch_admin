export const state = () => ({
  users: []
})

export const mutations = {}

export const getters = {}

export const actions = {
  verifyEmail({ commit }, VerifyData) {
    return this.$axios.post('admin/verify/email', VerifyData)
  },

  preVerifyEmail({ commit }, VerifyData) {
    return this.$axios.post('verify/email/pre', VerifyData).then(res => {
      return res.data
    })
  },

  register({ commit }, RegisterData) {
    return this.$axios.post('admin/register', RegisterData)
  },

  updateBasicProfile({ commit }, updateData) {
    return this.$axios.put('admin/users/' + updateData.id, updateData.data)
  },

  deleteAdmin({ commit }, DeleteData) {
    return this.$axios.delete(`admin/users/${DeleteData.id}`)
  },

  forgotPassword({ commit }, forgotData) {
    return this.$axios.post('password/forgot', forgotData)
  },

  resetPassword({ commit }, resetData) {
    return this.$axios.post('password/reset', resetData)
  }
}
