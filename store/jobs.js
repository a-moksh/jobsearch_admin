import store from '~/storeBaseModule'
import _ from 'lodash'
const resource = 'jobs'
const base = store({ resource })

export default _.merge(base, {
  actions: {
    getTitles({ commit, dispatch }) {
      return this.$axios.get('jobs/title').then(response => {
        return response.data
      })
    }
  }
})
