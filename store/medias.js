import store from '~/storeBaseModule'
import _ from 'lodash'
const resource = 'admin/images'
const base = store({ resource })

export default _.merge(base, {})
