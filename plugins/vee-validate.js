import Vue from 'vue'
import VeeValidate from 'vee-validate'
const dictionary = {
  en: {
    messages: {
      verify_password: 'The password must be alpha numeric and 8-15 length',
      katakana: 'Please enter only katakana characters',
      number_with_start_non_zero: 'please enter valid data.',
      romanji_number_only: 'Please enter only romanji half-width numbers'
    }
  },
  ja: {
    messages: {
      verify_password: 'パスワードは英数字で8-15の長さでなければなりません',
      katakana: 'カタカナで入力してください',
      number_with_start_non_zero: '入力データが無効です.',
      romanji_number_only: '半角数字のみを入力してください'
    }
  }
}
VeeValidate.Validator.localize(dictionary)
Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeField'
})

VeeValidate.Validator.extend('verify_password', {
  validate: value => {
    const strongRegex = new RegExp(
      '^(?=.*\\d)(?=.*[a-z])[\\w~@#$%^&*+=`|{}:;!.?\\"()\\[\\]-]+$'
    )
    return strongRegex.test(value) && value.length >= 8 && value.length <= 15
  }
})

VeeValidate.Validator.extend('katakana', {
  validate: value => {
    const katakanaReg = new RegExp('[ァ-ン 　]')
    const katakanaFullWidth = new RegExp('[ァーン 　]')
    const valueArr = value.split('')
    let test = true
    for (let i = 0; i < valueArr.length; i++) {
      if (test === false) return false
      test =
        (katakanaReg.test(valueArr[i]) ||
          katakanaFullWidth.test(valueArr[i])) &&
        !/^[a-zA-Z]+$/.test(valueArr[i])
    }
    return test
  }
})

VeeValidate.Validator.extend('number_with_start_non_zero', {
  validate: value => {
    const strongRegex = new RegExp('^[1-9][0-9]*')
    return strongRegex.test(value)
  }
})

VeeValidate.Validator.extend('romanji_number_only', {
  validate: value => {
    const strongRegex = new RegExp('^[0-9]+$')
    return strongRegex.test(value)
  }
})
