import _ from 'lodash'

export default function({ store, $axios, redirect }) {
  if (process.env.DEBUG) {
    $axios.interceptors.request.use(request => {
      console.log('Starting Request: ', {
        method: request.method,
        baseurL: request.baseURL,
        URL: request.url,
        params: request.params
      })
      return request
    })

    $axios.interceptors.response.use(response => {
      // console.log('Response: ', {'link': response.data.links.first })
      // response.data})
      return response
    })
  }

  $axios.interceptors.request.use(config => {
    if (
      !config.url.includes('maps.googleapis.com') &&
      !config.url.includes('amazonaws.com')
    ) {
      const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone
      config.headers['Accept-Language'] = store.state.locale
      if (!_.isEmpty(timeZone)) {
        config.headers['Time-Zone'] = timeZone
      }
    } else {
      delete config.headers.common.Authorization
    }
    return config
  })

  /*
  $axios.onRequest(config => {
    console.log('Making request to '+ config.url)
  })
  */

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      // redirect('/400')
    }
  })
}
