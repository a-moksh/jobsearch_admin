import moment from 'moment'

export default ({ store }, inject) => {
  inject('moment', moment)
}
