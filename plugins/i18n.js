import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)
const globalEn = require('~/locales/en/global.json')
const globalJa = require('~/locales/ja/global.json')
const pageContentEn = require('~/locales/en/pageContent.json')
const pageContentJa = require('~/locales/ja/pageContent.json')
export default ({ app, store }) => {
  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch
  app.i18n = new VueI18n({
    locale: store.state.locale,
    fallbackLocale: 'en',
    messages: {
      en: { global: globalEn, page: pageContentEn },
      ja: { global: globalJa, page: pageContentJa }
    }
  })

  app.i18n.path = (path, locale) => {
    const _locale = locale || store.state.locale
    let _link = path || app.router.currentRoute.fullPath
    if (path) {
      _link = `/${_locale}${_link}`
    }
    const re = new RegExp(`(^/)(.+?)(/.*)`)
    _link = _link.replace(re, `$1${_locale}$3`)
    return _link
  }
}
