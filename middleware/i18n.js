import _ from 'lodash'

export default function({ isHMR, app, store, route, params, error, redirect }) {
  const defaultLocale = app.i18n.fallbackLocale

  // If middleware is called from hot module replacement, ignore it
  if (isHMR) return

  // Get locale from params
  const locale = params.lang || defaultLocale

  if (!_.hasIn(store, `state.locales.${locale}`)) {
    return error({ message: 'This page could not be found.', statusCode: 404 })
  }
  if (route.fullPath.indexOf(`/${locale}`) < 0) {
    return redirect(`/${locale}${route.fullPath}`)
  }
  store.commit('SET_LOCALE', locale)
}
