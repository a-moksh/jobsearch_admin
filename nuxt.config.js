import dotenv from 'dotenv'
import pkg from './package'

dotenv.config()

export default {
    env: process.env,
    mode: 'universal',

    /*
     ** Headers of the page
     */
    head: {
        title: pkg.name,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'stylesheet',
                href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
            }
        ]
    },

    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: '#27B6C5',
        height: '4px'
    },

    /*
     ** Global CSS
     */
    css: ['~/assets/style/admin.css'],

    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '~/plugins/lodash.js',
        '~/plugins/axios.js',
        '~/plugins/i18n.js',
        '~/plugins/vee-validate.js',
        '~/plugins/fontawesome.js',
        '~/plugins/moment.js',
        '@/plugins/bootstrap-vue',
        { src: '~/plugins/dialog.js', ssr: false },
        { src: '~/plugins/datepicker.js', ssr: false },
        { src: '~plugins/echo.js', ssr: false }
    ],
    generate: {
        routes: ['/en', 'en/about', '/ja', '/ja/about']
    },
    toast: {
        position: 'bottom-right',
        duration: 4000,
        fullWidth: false
    },

    router: {
        base: '/',
        middleware: ['i18n'] // middleware all pages of the application
    },

    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: {
                        url: '/admin/login',
                        method: 'post',
                        propertyName: 'access_token'
                    },
                    logout: { url: '/logout', method: 'post' },
                    user: { url: '/admin/user', method: 'get', propertyName: 'data' }
                },
                tokenRequired: true,
                tokenType: 'Bearer'
            }
        },
        redirect: {
            login: false,
            logout: false,
            callback: false,
            home: false
        },
        localStorage: {
            prefix: 'auth.'
        },
        cookie: {
            prefix: 'auth.',
            options: {
                path: '/'
            }
        }
    },
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://github.com/nuxt-community/axios-module#usage
        '@nuxtjs/axios',
        // Doc: https://bootstrap-vue.js.org/docs/
        'bootstrap-vue/nuxt',
        '@nuxtjs/auth',
        '@nuxtjs/toast',
        'nuxt-fontawesome'
    ],

    fontawesome: {
        imports: [{
                set: '@fortawesome/free-solid-svg-icons',
                icons: ['fas']
            },
            {
                set: '@fortawesome/free-brands-svg-icons',
                icons: ['fab']
            }
        ]
    },

    /*
     ** Axios module configuration
     */
    axios: {
        // See https://github.com/nuxt-community/axios-module#options
        baseURL: process.env.API_URL
    },

    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        /*
        extend(config, ctx) {
          // Run ESLint on save
          if (ctx.isDev && ctx.isClient) {
            config.module.rules.push({
              enforce: 'pre',
              test: /\.(js|vue)$/,
              loader: 'eslint-loader',
              exclude: /(node_modules)/
            })
          }
        }
        */
    }
}