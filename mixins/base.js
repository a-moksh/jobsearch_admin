import _ from 'lodash'
import { Validator } from 'vee-validate'
import ja from 'vee-validate/dist/locale/ja'
import en from 'vee-validate/dist/locale/en'

export default function(resource) {
  return {
    head() {
      this.$store.commit('SET_PAGE_TITLE', this.pageHead('title'))
      return {
        title: this.pageHead('title')
      }
    },
    fetch({ store }) {
      // return store.dispatch('langAsync', 'navigation')
    },
    methods: {
      date_lang() {
        if (this.$store.getters.locale === 'ja') {
          return {
            days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            months: [
              '１月',
              '２月',
              '３月',
              '４月',
              '５月',
              '６月',
              '７月',
              '８月',
              '９月',
              '１０月',
              '１１月',
              '１２月'
            ],
            pickers: [
              'next 7 days',
              'next 30 days',
              'previous 7 days',
              'previous 30 days'
            ],
            placeholder: {
              date: 'Select Date',
              dateRange: 'Select Date Range'
            }
          }
        } else {
          return {
            days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            months: [
              'Jan',
              'Feb',
              'Mar',
              'Apr',
              'May',
              'Jun',
              'Jul',
              'Aug',
              'Sep',
              'Oct',
              'Nov',
              'Dec'
            ],
            pickers: [
              'next 7 days',
              'next 30 days',
              'previous 7 days',
              'previous 30 days'
            ],
            placeholder: {
              date: 'Select Date',
              dateRange: 'Select Date Range'
            }
          }
        }
      },
      showHideNoti() {
        this.$store.commit(
          'SET_NOTIFICATION_DRAWER',
          !(this.$store.getters.isAuthenticated
            ? this.$store.getters.notification_drawer
            : true)
        )
      },

      pageHead(key, props) {
        return this.$t('global.title')
      },

      pageContent(key, props) {
        const rootKey = _.get(
          this.$options,
          'config.contentRootKey',
          this.$store.getters.pageRootKey
        )
        if (!_.isEmpty(rootKey)) {
          const _key = `${rootKey}.${key}`
          return this.$t(_key, props)
        }
        const pageRootKey = _.get(this.$options, 'pageRootKey')
        if (!_.isEmpty(pageRootKey)) {
          return this.$t(`page.${pageRootKey}.content.${key}`, props)
        }
      },

      /**
       * display attribute
       * @param {string} group
       * @param {string} value
       * @returns Array {*}
       */
      attribute(group, value) {
        return this.$store.getters.attribute({ group, value })
      },

      /**
       * display attribute
       * @param {string} group
       * @param {string} text
       * @returns Array {*}
       */
      attributeByText(group, text) {
        const items = this.$store.getters.attribute({ group })
        return _.find(items, { text })
      },

      veevalidateLocalize() {
        if (this.$store.getters.locale === 'en') {
          Validator.localize('en', en)
        } else {
          Validator.localize('ja', ja)
        }
      },

      openModal(item, modal, action) {
        this.$dlg.alert('Do you really want to leave?', {
          messageType: 'confirm',
          language:
            this.$store.getters.locale !== 'ja'
              ? this.$store.getters.locale
              : 'jp',
          callback: function(item, modal, action) {
            // do something when you click cancel button
          }
        })
      }
    }
  }
}
