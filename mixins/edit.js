import _ from 'lodash'
import base from '~/mixins/base'

export default function(resource) {
  return _.merge({}, base(resource), {
    validate: function({ params }) {
      // return /^\d+$/.test(params.id)
      return true
    },
    methods: {
      langAttribute(attr) {
        return _.filter(this.attribute(attr), function(el) {
          return !(parseInt(el.value) === 0 || parseInt(el.value) === 1)
        })
      }
    }
  })
}
