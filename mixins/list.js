import _ from 'lodash'

import base from '~/mixins/base'

export default function(resource) {
  return _.merge({}, base(resource), {
    methods: {
      resetSearchParameters() {
        _.each(this.searchParameters, (value, key) => {
          if (key !== 'user_type') this.searchParameters[key] = null
        })
        this.skills = []
        this.refreshTable()
      },
      makeQuery(searchParams) {
        const searchArr = []
        _.each(searchParams, (value, key) => {
          if (value !== null && value !== '00') {
            if (key === 'created_at') {
              value = new Date(value).toDateString()
            }
            if (key === 'dob' || key === 'visa_issue_date') {
              value = this.$moment(value).format('YYYY-MM-DD')
            }
            searchArr.push(`${key}=${value}`)
          }
        })
        return `?${_.join(searchArr, '&')}`
      },
      getTableData(ctx, callback, searchParams) {
        this.isBusy = true
        if (ctx.sortBy) searchParams.orderby = ctx.sortBy
        if (ctx.sortDesc === false) {
          searchParams.direction = 'desc'
          this.sortOrder = true
        } else {
          searchParams.direction = 'asc'
          this.sortOrder = false
        }
        const params = this.makeQuery(searchParams)
        this.$axios
          .get(`/${resource}${params}`)
          .then(response => {
            this.meta = response.data.meta
            this.isBusy = false
            callback(response.data.data)
          })
          .catch(() => {
            this.isBusy = false
            //  callback()
          })
      },
      refreshTable() {
        this.$refs.table.refresh()
      }
    },
    watch: {
      'searchParameters.page': function(page) {
        this.searchParameters.page = page
        this.refreshTable()
      }
    }
  })
}
